# JSON

JSON stands for **JavaScript Object Notation**.
It is a lightweight data interchange format.

## Some Important JSON Rules

It is important to note the following when working with JSON files

* All keys must be wrapped in double quotes
* All string values must be wrapped in double quotes
* The last key/value pair in a JSON object cannot be followed by a comma
* Comments are not allowed in JSON files

**Important:**

Always validate your JSON files. You can use the [jsonlint](https://jsonlint.com/?code=) online validator
