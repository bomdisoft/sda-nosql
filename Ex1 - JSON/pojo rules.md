# POJO

POJO stands for Plain Old Java Objects.

# Some Important POJO Rules

It is important to note the following when creating POJO classes

* The class must be public
* The class must have public default constructor
* Properties must be private
* Every property must have public getter and setter methods
* The class can also have args constructor (optional)
