package com.softwaredevelopmentacademy.encapsulation;

public class Post {
    private String title;
    private String body;
    private String author;
    private int likes;
    private boolean isPublished;

    // getters
    public String getTitle() { return title; }
    public boolean isPublished() { return isPublished; }
    public String getAuthor() { return author; }
    public int getLikes() { return likes; }
    public String getBody() { return body; }

    // setters
    public void setBody(String body) { this.body = body; }
    public void setTitle(String title) { this.title = title; }
    public void setAuthor(String author) { this.author = author; }
    public void setLikes(int likes) { this.likes = likes; }
    public void setPublished(boolean published) { isPublished = published; }

    @Override
    public String toString() {
        return "{\n\"title\": " + title + ",\n\"body\": " + body + ",\n\"author\": " + author
                + ",\n\"likes\": " + likes + ",\n\"isPublished\": " + isPublished + "\n}";
    }
}
