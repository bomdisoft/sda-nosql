package com.softwaredevelopmentacademy.encapsulation;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

public class JsonToPojo {
    public static void main(String[] args){
        Reader reader;

        try {
            reader = new FileReader("post.json"); // NB: insert the relative/absolute path to your json file here
            Gson gson = new Gson(); // You can use other libraries like Jackson

            Post postData = gson.fromJson(reader, Post.class);
            System.out.println(postData);
        }
        catch (FileNotFoundException e) { e.printStackTrace(); }
    }
}
