import java.util.ArrayList;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private ArrayList<String> children = new ArrayList<>();

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public void setAge(int age){
        this.age = age;
    }
    
    public void addChild(String childName){
        this.children.add(childName);
    }

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public int getAge(){
        return this.age;
    }
    
    public String[] getChildren(){
        return this.children;
    }
}
