db.createCollection('students')

db.students.insertMany([
	{ _id : 1, grades : [ 95, 92, 90 ]},
	{ _id : 2, grades : [ 98, 100, 102 ]},
	{ _id : 3, grades : [ 95, 110, 100 ]}
])

////// EX1
// $[] - acts as a placeholder
// $set - writes a value to a field
 db.students.updateOne(
	{ grades: { $gt: 100 }},
	{ $set: { "grades.$[element]" : 100 }},
	{ arrayFilters: [{ "element": { $gt: 100 }}]}
 )

 // $push - adds an item to an array
db.students.updateOne(
	{ _id: 1 },
	{ $push: { grades: 50 }}
)
db.students.updateOne( 
	{ _id: 3 },
	{ $push: { grades: [ 50, 60, 70 ]}} // ???
)
db.students.updateOne(
	{ _id: 2 },
	{ $push: { grades: { $each: [ 90, 92, 85 ]}}}
)

 // $pop - removes the first or last item of an array
db.students.updateOne(
	{ _id: 3 }, 
	{ $pop: { grades: -1 }}
)
db.students.updateOne(
	{ _id: 3 }, 
	{ $pop: { grades: 1 }}
)

////// EX2
 db.students2.insertMany([
	{
	   _id : 1,
	   grades : [
		  { grade : 80, mean : 75, std : 6 },
		  { grade : 85, mean : 90, std : 4 },
		  { grade : 85, mean : 85, std : 6 }
	   ]
	},
	{
	   _id : 2,
	   grades : [
		  { grade : 90, mean : 75, std : 6 },
		  { grade : 87, mean : 90, std : 3 },
		  { grade : 85, mean : 85, std : 4 }
	   ]
	}
 ])

/*QUESTION

Write a query update the mean value of every grade to 100 where the grade value is greater or equal to 85

*/


// EX3
db.students3.insertMany([
	{
		"_id": 1,
		"alias": [ "The American Cincinnatus", "The American Fabius" ],
		"nmae": { "first" : "george", "last" : "washington" }
	},	
	{
		"_id": 2,
		"alias": [ "My dearest friend" ],
		"nmae": { "first" : "abigail", "last" : "adams" }
	},
	{
		"_id": 3,
		"alias": [ "Amazing grace" ],
		"nmae": { "first" : "grace", "last" : "hopper" }
	}
])

// $rename - renames a field
db.students3.updateMany(
	{}, 
	{ $rename: { "nmae": "name" }}
)


/*QUESTION

write a query update 'first' of the first student in students3 collection to 'fname'

*/



// OTHER OPERATORS

// $currentDate - sets the value of a field to current date, either as a Date or a Timestamp

// $inc - increments the value of the field by the specified amount

// $min - only updates the field if the specified value is less than the existing field value

// $max - only updates the field if the specified value is greater than the existing field value

// $mul - multiplies the value of the field by the specified amount

// $unset - removes the specified field from a document

// $ - acts as a placeholder to update the first element that matches the query condition
 
// $addToSet - adds elements to an array only if they do not already exist in the set

// $pull - removes all array elements that match a specified query

// $pullAll - removes all matching values from an array


///SOLUTION
db.students2.updateMany(
	{},
	{ $set: { "grades.$[elem].mean" : 100 }},
	{ arrayFilters: [ { "elem.grade": { $gte: 85 }}]}
)

 
db.students3.update(
	{ _id: 1 }, 
	{ $rename: { "name.first": "name.fname" }}
)
