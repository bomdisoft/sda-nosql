// show help
help 

// show help for database methods
db.help() 

// print a list of all databases on the server
show dbs 

// switch current database to 'local'. The mongo shell variable db is set to the current database ('local')
use local 

// print a list of all collections for the current database ('local')
show collections

//create new database
use test

// show all collections in 'test'
show collections

// show help on collection methods (existing collection or a non-existing collection)
db.test.help() 

// create collections in test
db.createCollection("post")
db.createCollection("user")

// drop collection from test
db.post.drop()

// create new users for db and assign roles
db.createUser({
	user: "Jack",
	pwd: "superStrongPassword",
	roles: ["read"]
});
db.createUser({
	user: "Paul", 
	pwd: "Password",
	roles: ["dbAdmin"]
});
db.createUser({
	"_id":"25556",
	user: "Jack",
	pwd: "pass121",
	roles: ["readWrite", "dbAdmin"],
	customData: {

	}
});

// drop user from db
db.dropUser('Jack')

// Other roles
"userAdmin"
"dbOwner"
"clusterAdmin"
"clusterManager"
"clusterMonitor"

// More info on roles here: https://docs.mongodb.com/manual/reference/built-in-roles/
