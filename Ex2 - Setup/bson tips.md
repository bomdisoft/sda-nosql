# BSON

BSON stands for Binary JSON.
It is a way to represent JSON in binary format for databases. 
This is necessary for a couple of reasons including

* Fast scannability
* Support for data types
	* Date
	* ObjectId
	* BinData
