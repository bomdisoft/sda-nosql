// inserts a document into a user collection
db.user.insertOne({
	name: 'Ryan',
	age: 15,
	isActive: true,
	hobbies: ['swimming', 'running', 'sky diving']
})

// selects documents in a user collection based on the filter and returns a cursor to the selected documents
db.user.find()
db.user.find().pretty()

// inserts multiple documents into a user collection
db.user.insertMany([
	{
		name: 'Mike Ross',
		age: 32,
		isActive: true
	},
	{
		_id: 0,
		name: 'Mike Ross',
		age: 32,
		isActive: false
	},
	{
		name: 'Mike Ross',
		hobbies: ['video games']
	}
])

// select document in a user collection based on the filter
db.user.findOne()

db.user.findOne({
	_id: 0
})

db.user.findOne({
	name: 'Mike Ross'
})

db.user.findOne({
	name: 'Mike Ross',
	isActive: false,
})

// updates a single document within the user collection based on the filter
db.user.updateOne(
	{
		_id: ObjectId("5c48d9198048c0297badcf41")
	},
	{
		$set: {
			name: "Mary Jane"
		}
	}
) 

// no document found to update?
db.user.updateOne(
	{
		_id: 666
	},
	{
		$set: {
			name: "Tommy Jackson",
			isActive: true,
			occupation: 'Actor'
		}
	}
) 
// no document found to update? with upsert
db.user.updateOne(
	{
		_id: 666
	},
	{
		$set: {
			name: "Tommy Jackson",
			isActive: true,
			occupation: 'Actor'
		}
	},
	{
		upsert: true
	}
) 

db.user.count()

// updates all documents within the user collection that match the filter
db.user.updateMany(
	{
		name: "Mike Ross"
	},
	{
		$set: {
			name: "Mike Rose",
			gender: "Female"
		}
	}
) 
// update entire document
db.user.updateMany(
	{},
	{
		$set: {
			isActive: true
		}
	}
) 



/*QUESTION

1) Add a field named "country" to all existing users
2) Remove the "occupation" field from the user with _id = 666

*/



// replaces a single document within the user collection based on the filter
db.user.replaceOne(
	{
		_id: 0
	},
	{
		name: 'Peter Parker',
		profession: 'Spiderman'
	}
) 

// removes a single document from a user collection based on the filter
db.user.deleteOne({
	_id: 0
}) 

// removes all documents that match the filter from a user collection
db.user.deleteMany({
	age: 15
}) 
