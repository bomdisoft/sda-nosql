db.createCollection('stores')

db.stores.insertMany([
	{
		_id: 1,
		fruits: [  "pears", "grapes", "apples", "oranges" ],
		vegetables: [ "celery", "squash" ]
	},
	{
		_id: 2,
		fruits: [ "plums", "oranges", "kiwis", "bananas" ],
		vegetables: [ "broccoli", "zucchini", "onions", "carrots" ]
	},
	{
		_id: 3,
		fruits: [ "avocados" ]
	}
])

db.stores.insertMany([
	{
		_id: 5,
		fruits: [  "pears", "apples" ],
		vegetables: [ "onions", "squash" ]
	},
])

// $in - matches any of the values specified in an array
db.stores.find({ 
	fruits: { $in: [ "avocados", "grapes" ]  } 
})

// $or - joins query clauses with a logical OR returns all documents that match the conditions of either clause
// $size - selects documents if the array field is a specified size
db.stores.find({ 
	$or: [ 
		{ fruits: { $size: 1 } }, 
		{ fruits: "kiwis" } 
	] 
})

// $and - joins query clauses with a logical AND returns all documents that match the conditions of both clauses
db.stores.find({ 
	$and: [ 
		{ fruits: { $size: 4 } }, 
		{ vegetables: { $size: 4 } } 
	] 
})


/*QUESTION

Write a query to retrieve all stores with apples or oranges in their fruits section, 
or onions in their vegetables section

*/


// $Push - adds item to array
db.stores.updateMany(
    { _id: 3 },
    { 
		$push: { 
			fruits: "Mangoes"
		}
	}
)

// $pop - removes values from an array
db.stores.updateMany(
	{_id: 3},
	{
		$pop: {
			fruits: -1
		}
	}
)

// $Pull - removes item from array
db.stores.updateMany(
    {},
    { 
		$pull: { 
			fruits: { 
				$in: [ "apples", "oranges" ] 
			}, 
			vegetables: "carrots" 
		}
	}
)

db.stores.drop()


//OTHER SELECTORS
// $exists - matches documents that have the specified field

// $where - matches documents that satisfy a JavaScript expression

// $text - performs text search

// $eq - matches values that are equal to a specified value

// $gt - matches values that are greater than a specified value

// $gte - matches values that are greater than or equal to a specified value

// $lt - matches values that are less than a specified value

// $lte - matches values that are less than or equal to a specified value

// $ne - matches all values that are not equal to a specified value

// $nin - matches none of the values specified in an array

// $not - inverts the effect of a query expression and returns documents that do not match the query expression

// $nor - joins query clauses with a logical NOR returns all documents that fail to match both clauses

// $type - selects documents if a field is of the specified type

// $expr - allows use of aggregation expressions within the query language

// $mod - performs a modulo operation on the value of a field and selects documents with a specified result

// $regex - selects documents where values match a specified regular expression

// $all - matches arrays that contain all elements specified in the query.

// $elemMatch - selects documents if element in the array field matches all the specified $elemMatch conditions


//// SOLUTION
db.stores.find({
	$or: [ 
		{
			fruits: { 
				$in: [ "apples", "oranges" ] 
			} 
		}, 
		{ vegetables: "carrots" } 
	] 
})
